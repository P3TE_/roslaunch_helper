# README #

This repo contains scripts that may help with your roslaunch needs.
For me, I needed the following:
* Have roslaunch launch files in a specific order.
* Have launchfiles wait for topics to start before beginning.
* Restart a roslaunch if a node fails to start after a given time.
* Kill all ros related stuff simply.

### License ###
* The contents of this repo is provided as open source under the Apache-2.0 license, read the LICENSE file for more information.
* Feel free to make forks and pull requests to the main repo.

### How to use ###
This repo has scripts that will help with ros launches. The following is a explanation of each script and input arguments:
add_active_pid.sh PID - This script takes a PID as argument 1 and appends it to the ACTIVE_PIDS file which will be created at the root of the git repository.
are_topics_published.sh [/topic1 /topic2 ...] - Will return 0 if all topics are published, 1 if any of the topics are not currently visible in 'rostopic list'


### Who do I talk to? ###

* Peter Smith - p96.smith@qut.edu.au