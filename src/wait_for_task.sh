#!/bin/bash

ARGUMENT_COUNT=$#


TEST_SUCCESS_SCRIPT=$1
RETRY_ATTEMPT_COUNT=$2

re='^[1-9]+([0-9]+)?$'

EXIT_CODE=1


if [ $ARGUMENT_COUNT -ne 2 ]; then
	
	echo "Usage: $0 check_if_ready_script attempts"
	EXIT_CODE=-1
elif ! [[ $RETRY_ATTEMPT_COUNT =~ $re ]]; then

	echo "attempts MUST be a positive integer, received: $RETRY_ATTEMPT_COUNT" >&2; 
	EXIT_CODE=-2

else

	CURRENT_CHECK_COUNT="0"

	EXIT_CODE=1

	while [ $CURRENT_CHECK_COUNT -lt $RETRY_ATTEMPT_COUNT ]; do

		${TEST_SUCCESS_SCRIPT}
		SCRIPT_EXIT_CODE=$?
		
		if [ $SCRIPT_EXIT_CODE -eq 0 ]; then
			echo "Test succeed!"
			EXIT_CODE=0
			CURRENT_CHECK_COUNT=$RETRY_ATTEMPT_COUNT
		else
			echo "[${CURRENT_CHECK_COUNT}] - Test failed, trying again in 1 second..."
			CURRENT_CHECK_COUNT=$((CURRENT_CHECK_COUNT + 1))
			sleep 1
		fi

	done

	#Timed out and didn't succeed.
	if [ $EXIT_CODE -eq 1 ]; then
		echo "Timed out, test didn't succeed."
	fi

fi

exit $EXIT_CODE