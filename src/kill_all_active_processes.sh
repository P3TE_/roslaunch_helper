#!/bin/bash

#cd to script directory:
cd "$(dirname "$0")"

ALL_PROCESSES=`cat ../ACTIVE_PIDS`

(
IFS='
'

PROCESSES_KILLED=0

for x in $ALL_PROCESSES; do 
	echo "Killing: $x"
	kill $x
	PROCESSES_KILLED=$((PROCESSES_KILLED + 1))
done

if [ $PROCESSES_KILLED -eq 0 ]; then
	echo "Nothing to kill"
	./clear_active_pids.sh
else
	echo "Clearing active pids."
	./clear_active_pids.sh
fi

)