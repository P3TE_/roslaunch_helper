#!/bin/bash

ARGUMENT_COUNT=$#

if [ $ARGUMENT_COUNT -lt 3 ]; then
	echo "usage $0 SCRIPT_TO_RUN TEST_SUCCESS_SCRIPT MAXIMUM_WAIT_TIME_SECONDS [RETRY_X_TIMES]"
	exit -1
elif [ $ARGUMENT_COUNT -gt 4 ]; then
	echo "usage $0 SCRIPT_TO_RUN TEST_SUCCESS_SCRIPT MAXIMUM_WAIT_TIME_SECONDS [RETRY_X_TIMES]"
	exit -2
fi

SCRIPT_TO_RUN=$1
TEST_SUCCESS_SCRIPT=$2
MAXIMUM_WAIT_TIMEZEHGNEROUS=$3


re='^[1-9]+([0-9]+)?$'
if ! [[ $3 =~ $re ]] ; then
	echo "MAXIMUM_WAIT_TIME_SECONDS MUST be a positive integer > 0, received: $3" >&2; 
	exit -1
fi

RETRY_X_TIMES="0"
if [ $ARGUMENT_COUNT -gt 3 ]; then
	re='^[0-9]+$'
	if ! [[ $4 =~ $re ]] ; then
	   echo "RETRY_X_TIMES MUST be a positive integer >= 0, received: $4" >&2; 
	   echo "Falling back to using RETRY_X_TIMES = 0"
	else
		RETRY_X_TIMES=$4
	fi
fi

RETRY_COUNT=0

while [ $RETRY_COUNT -le $RETRY_X_TIMES ]; do
	
	${SCRIPT_TO_RUN} &
	SCRIPT_PID=$!
	$(dirname "$0")/add_active_pid.sh $SCRIPT_PID


	TEST_COMMAND="$(dirname "$0")/wait_for_task.sh '"$TEST_SUCCESS_SCRIPT"' $MAXIMUM_WAIT_TIMEZEHGNEROUS"
	echo "Running: $TEST_COMMAND"
	eval $TEST_COMMAND
	TEST_EXIT_CODE=$?
	echo "TEST_COMMAND run, exit code = $TEST_EXIT_CODE"

	if [ $TEST_EXIT_CODE -eq 0 ]; then
		echo "Success"
		exit 0
	else
		RETRY_COUNT=$((RETRY_COUNT + 1))

		if [ $RETRY_COUNT -le $RETRY_X_TIMES ]; then

			echo "Attempting to restart: ${SCRIPT_TO_RUN}"
			kill $SCRIPT_PID
			$(dirname "$0")/remove_from_pids.sh $SCRIPT_PID
			sleep 1

		fi
	fi
done

echo "Timed out."
kill $SCRIPT_PID
$(dirname "$0")/remove_from_pids.sh $SCRIPT_PID
exit 1

