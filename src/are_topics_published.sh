#!/bin/bash

ROS_TOPIC_RESULT=`rostopic list 2>/dev/null`

ARGUMENT_COUNT=$#

TOPICS_FOUND=0

(
IFS='
'
for x in $ROS_TOPIC_RESULT; do 
	COUNTER=1
	while [  $COUNTER -le $ARGUMENT_COUNT ]; do
		eval ARG_X='$'$COUNTER
		if [ "$ARG_X" = "$x" ]; then
			TOPICS_FOUND=$((TOPICS_FOUND + 1))
		fi
		COUNTER=$((COUNTER + 1))
	done
done

if [ $TOPICS_FOUND -eq $ARGUMENT_COUNT ]; then
	echo "All topics found."
	exit 0
else
	echo "Not all topics found."
	exit 1
fi
)

#Using a subshell, so exit with the exitcode of the subshell:
exit $?


