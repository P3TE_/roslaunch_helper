#!/bin/bash

SILENT=0
while test $# -gt 0; do
    case "$1" in
        -s|--silent)
			SILENT=1
			;;
		*)
			echo "Unknown flag: $1"
			;;
	esac
	shift
done


rostopic list > /dev/null 2>&1
EXIT_STATUS=$?

#The important thing is the exit status, but this is just helpful output.
if [ $SILENT -eq 0 ]; then
	if [ $EXIT_STATUS -eq 0 ]; then
		echo "roscore ready"
	else
		echo "roscore not available"
	fi
fi


exit $EXIT_STATUS